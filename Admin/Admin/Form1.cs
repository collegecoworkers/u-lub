﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Admin
{
    public partial class Form1 : Form
    {
        static private string delimiter = ";;;S";
        static private int DEFAULT_BUFLEN = 256;

        static private Socket Client;
        private IPAddress ip = null;
        private int port = 7770;
        private bool is_disconected = false;
        private Thread th;
   
        public Form1()
        {
            InitializeComponent();
            try
            {
                var sr = new StreamReader(@"ClientInfo/data_info.txt");
                string buffer = sr.ReadToEnd();
                sr.Close();
                ip = IPAddress.Parse(buffer.Trim());
                info.ForeColor = Color.Green;
                info.Text = "Подключено к " + ip.ToString() + ":" + port.ToString();
            }
            catch (Exception ex)
            {
                info.ForeColor = Color.Red;
                info.Text = "Настройки не найдены";
                ShowSett();
            }
            
            try
            {
                init();
            }
            catch (Exception ex)
            {
                info.ForeColor = Color.Red;
                info.Text = ex.Message;
            }
        }

        string iw_genNam(string id){
            return "PK " + id;
        }

        bool iw_has(string id){
            for (int n = listBoxItems.Items.Count - 1; n >= 0; --n)
            {
                // if (listBoxItems.Items[n].Text == iw_genNam(id))
                if (listBoxItems.Items[n].ToString() == iw_genNam(id))
                {
                    return true;
                }
            }
            return false;
        }

        void iw_add(string id){
           listBoxItems.Items.Add(iw_genNam(id));
        }

        void iw_rem(string id){
           for (int n = listBoxItems.Items.Count - 1; n >= 0; --n)
           {
                // if (listBoxItems.Items[n].Text == iw_genNam(id))
                if (listBoxItems.Items[n].ToString() == iw_genNam(id))
                {
                    iw_remAt(n);
                }
            }
        }

        void iw_remAt(int index){
           listBoxItems.Items.RemoveAt(index);
        }

        void ShowSett()
        {
            Settings s = new Settings();
            s.Show();
        }

        void EnabledBtns(bool s){
            off.Enabled = s;
            restart.Enabled = s;
            closefrom.Enabled = s;
        }

        void init()
        {
            EnabledBtns(false);
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (ip != null)
            {
                Client.Connect(ip, port);
                th = new Thread(delegate () { RecvMessage(); });
                th.Start();
                SendMessage("to_admin");
            }
        }
 

        void executeComand(string mes)
        {

            string[] data = mes.Split('|');

            if(data.Length <= 1) return;
      
            string id = data[0];
            string val = data[1];

            if(id == "close"){
                if(iw_has(val)){
                    iw_rem(val);
                }
                return;
            }

            if(!iw_has(id) && val == "1"){
                iw_add(id);
            }

        }

        void SendMessage(string message)
        {
            if (message.Trim() == "")
                return;

            byte[] buffer = new byte[DEFAULT_BUFLEN];
            buffer = Encoding.UTF8.GetBytes(message + delimiter);
            Client.Send(buffer);
        }

        void SetDisconected()
        {
            Log("SetDisconected");
            info.ForeColor = Color.Red;
            info.Text = "Disconected";
            EnabledBtns(false);
        }

        void RecvMessage()
        {
            byte[] buffer = new byte[DEFAULT_BUFLEN];

            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = 0;
            }

            for (; ; )
            {
                try
                {
                    if (Client.Connected){
                        Client.Receive(buffer);
                        string message = Encoding.UTF8.GetString(buffer);
                        int count = message.IndexOf(delimiter);
                        if (count == -1)
                        {
                            continue;
                        }
                        string clear_message = "";

                        for (int i = 0; i < count; i++)
                        {
                            clear_message += message[i];
                        }
                        for (int i = 0; i < buffer.Length; i++)
                        {
                            buffer[i] = 0;
                        }

                        this.Invoke((MethodInvoker)delegate ()
                        {
                           executeComand(clear_message);
                        });
                    } else {
                        if(!is_disconected){
                            is_disconected = !is_disconected;
                            SetDisconected();
                            break;
                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }

        void Log(string mes)
        {
            System.Diagnostics.Debug.WriteLine(mes.ToString());
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void авторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Umbokc.\n 2018");
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSett();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BeforeClose();
            Application.Exit();
        }

        void createCommand(string comm){
            try
            {
                string curItem = listBoxItems.SelectedItem.ToString();
                SendMessage(comm + "|" + curItem.Replace("PK ", ""));
            }
            catch (Exception ex)
            {
            }
        }

        private void off_Click(object sender, EventArgs e)
        {
            createCommand("off");
        }

        private void restart_Click(object sender, EventArgs e)
        {
            createCommand("reboot");
        }

        private void BeforeClose()
        {
            if (th != null) th.Abort();
            if (Client != null) Client.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeClose();
        }

        private void listBoxItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void listBoxItems_SelectedValueChanged(object sender, EventArgs e)
        {
            EnabledBtns(!false);
        }

        private void closefrom_Click(object sender, EventArgs e)
        {
            createCommand("close");
        }
    }
}
