﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Client
{
    public partial class Form1 : Form
    {
        static private string delimiter = ";;;S";
        static private int DEFAULT_BUFLEN = 256;

        static private Socket Client;
        private IPAddress ip = null;
        private int port = 7770;
        private bool is_disconected = false;
        private static int my_id = -1;
        private Thread th;
        private Thread th2;

        public Form1()
        {
            InitializeComponent();
            try
            {
                var sr = new StreamReader(@"ClientInfo/data_info.txt");
                string buffer = sr.ReadToEnd();
                sr.Close();
                ip = IPAddress.Parse(buffer.Trim());
                infoPanel.ForeColor = Color.Green;
                infoPanel.Text = "Подключено к " + ip.ToString() + ":" + port.ToString();
                init();
            }
            catch (Exception ex)
            {
                getid.Enabled = false;
               // MessageBox.Show("Ошибка: " + );
                infoPanel.ForeColor = Color.Red;
                infoPanel.Text = ex.Message;
                ShowSett();
            }
        }

        void SendMessage(string message)
        {
            if (message.Trim() == "")
                return;

            byte[] buffer = new byte[DEFAULT_BUFLEN];
            buffer = Encoding.UTF8.GetBytes(message + delimiter);
            Client.Send(buffer);
        }

        void ShowSett()
        {
            Settings s = new Settings();
            s.Show();
        }

        void check_id(string mes){
            try
            {
                my_id = int.Parse(mes);
                idLabel.ForeColor = Color.Green;
                idLabel.Text = "Ваш ID " + my_id.ToString();
                getid.Enabled = false;
            }
            catch (Exception ex)
            {
                SendMessage("getid");
            }
        }

        void executeComand(string command)
        {
            if(command == "off"){
                infoPanel.Text += "\nКоманда: off";
                Log("off here");
                var psi = new ProcessStartInfo("shutdown", "/s /t 0");
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                Process.Start(psi);
            } else if(command == "reboot"){
                infoPanel.Text += "\nКоманда: reboot";
                Log("reboot here");
                Process.Start("shutdown", "/r /t 0");
             } else if(command == "close"){
                infoPanel.Text += "\nКоманда: close";
                Log("close here");
                BeforeClose();
                Application.Exit();
            } else {
                check_id(command);
            }

        }

        void Log(string mes)
        {
            System.Diagnostics.Debug.WriteLine(mes);
        }

        void SendOk()
        {
            for (; ; )
            {
                try
                {
                   SendMessage("1");
                   Thread.Sleep(75);
                }
                catch (Exception ex)
                {
                }
            }
        }

        void SetDisconected()
        {
            Log("SetDisconected");
            infoPanel.ForeColor = Color.Red;
            infoPanel.Text = "Disconected";
        }

        void RecvMessage()
        {
            byte[] buffer = new byte[DEFAULT_BUFLEN];

            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = 0;
            }

            for (; ; )
            {
                try
                {
                    if (Client.Connected){
                        Client.Receive(buffer);
                        string message = Encoding.UTF8.GetString(buffer);
                        int count = message.IndexOf(delimiter);
                        if (count == -1)
                        {
                            continue;
                        }

                        string clear_message = "";

                        for (int i = 0; i < count; i++)
                        {
                            clear_message += message[i];
                        }
                        for (int i = 0; i < buffer.Length; i++)
                        {
                            buffer[i] = 0;
                        }

                        this.Invoke((MethodInvoker)delegate ()
                        {
                            executeComand(clear_message);
                        });
                    } else {
                        if(!is_disconected){
                            is_disconected = !is_disconected;
                            SetDisconected();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        void init()
        {
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (ip != null)
            {
                Client.Connect(ip, port);
                th = new Thread(delegate () { RecvMessage(); }); th.Start();
                th2 = new Thread(delegate () { SendOk(); }); th2.Start();
            }
        }

        private void vtyToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSett();
        }

        private void авторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Umbokc.\n2018");
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BeforeClose();
            Application.Exit();
        }
        
        private void BeforeClose()
        {
            if (th != null) th.Abort();
            if (th2 != null) th2.Abort();
            if (Client != null) Client.Close();
        }
  
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeClose();
        }

        private void getid_Click(object sender, EventArgs e)
        {
            SendMessage("getid");
        }
    }
}
